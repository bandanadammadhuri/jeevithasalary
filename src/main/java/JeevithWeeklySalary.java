import java.util.Scanner;

public class JeevithWeeklySalary {
    public static void main(String[] args) {
        int sun,mon,tue,wed,thurs,fri,sat;
        int bonus, bonusRate, sunSalary,monSalary,tueSalary,wedSalary,thursSalary,friSalary,satSalary,weeklyBonus=0;
        int totalHours,totalSumSalary;
        Scanner scanner = new Scanner(System.in);
        sun = scanner.nextInt();
        mon = scanner.nextInt();
        tue = scanner.nextInt();
        wed = scanner.nextInt();
        thurs = scanner.nextInt();
        fri = scanner.nextInt();
        sat = scanner.nextInt();

        sunSalary = sun * 150;
        satSalary = sat * 125;

        if(mon <= 8) {
            monSalary = mon * 100;
        }
        else {
            bonus = mon - 8;
            bonusRate = bonus * 115;
            monSalary = bonusRate + 800;
        }
        if(tue <= 8){
            tueSalary = tue * 100;
        }
        else {
            bonus = tue - 8;
            bonusRate = bonus * 155;
            tueSalary = bonusRate + 800;
        }
        if(wed <= 8) {
            wedSalary = wed * 100;
        }
        else {
            bonus = wed - 8;
            bonusRate = bonus * 115;
            wedSalary = bonusRate + 800;
        }
        if(thurs <= 8) {
            thursSalary = thurs * 100;
        }
        else{
            bonus = thurs - 8;
            bonusRate = bonus * 115;
            thursSalary = bonusRate + 800;
        }
        if(fri <= 8) {
            friSalary = fri * 100;
        }
        else{
            bonus = fri - 8;
            bonusRate = bonus * 115;
            friSalary = bonusRate + 800;
        }
        totalHours = mon+tue+wed+thurs+fri;
        if(totalHours > 40) {
            totalHours = totalHours - 40;
            weeklyBonus = totalHours * 25;
        }
        totalSumSalary = sunSalary+monSalary+tueSalary+wedSalary+thursSalary+friSalary+satSalary+weeklyBonus;
        System.out.println(totalSumSalary);
    }
}
